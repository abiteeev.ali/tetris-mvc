import pygame
pygame.init()
from utils.variables import UIVariables, Configs

# a view for the game
class GameView:

    # initializes the game view
    def __init__(self, name, gameboard,  columns, rows):
        self.gameboard = gameboard
        self.title = name + "Tetris"
        self.displayWidth = Configs.gameboardWidth + 4
        self.displayHeight = rows * Configs.blockSize + 4
        # starting x coordinate for the sidebar
        self.gameDisplay = pygame.display.set_mode((self.displayWidth, self.displayHeight))
        self.gameDisplay.fill((0,0,0))
        pygame.display.set_caption(self.title)
        self.columns = columns
        self.rows = rows

    # clears the gameboard
    def clear(self):
        display = pygame.display.get_surface()
        display.fill((0, 0, 0))

    # draw the current active piece on the board
    def drawCurrentPiece(self):
        coord = self.gameboard.getActiveCoord()
        tetromino = self.gameboard.getActivePieceMatrix()
        if not self.gameboard.isGameOver():
            for r in range(4):
                for c in range(4):
                    dx = coord[0] * Configs.blockSize + (c * Configs.blockSize) + 2
                    dy = coord[1] * Configs.blockSize + (r * Configs.blockSize) + 2
                    if tetromino[r][c] != 0:
                        pygame.draw.rect(
                            self.gameDisplay,
                            UIVariables.tetrominoColors,
                            (dx + 2, dy + 2, Configs.blockSize - 4, Configs.blockSize - 4)
                        )


    # draws the game board with tiles
    def drawGameboard(self):
        for r in range(self.rows):
            for c in range(self.columns):
                currentBlockValue = self.gameboard.getBlock(r, c)
                color = UIVariables.gray
                if currentBlockValue != 0:
                    color = UIVariables.tetrominoColors
                dx = c * Configs.blockSize + 2
                dy = r * Configs.blockSize + 2
                pygame.draw.rect(
                    self.gameDisplay,
                    color,
                    (dx + 2, dy + 2, Configs.blockSize - 4, Configs.blockSize - 4))

    # draw game over
    def drawGameOver(self):
        self.drawGameboard()
        gameoverText = UIVariables.fontBold.render("GAMEOVER", 1, (0, 0, 0))
        gameoverScreen = pygame.Surface((Configs.gameboardWidth + 4, self.displayHeight))
        gameoverScreen.fill((255, 255, 255, 230))
        self.gameDisplay.blit(gameoverScreen, (0, 0))
        self.gameDisplay.blit(gameoverText, (Configs.blockSize * 1, self.displayHeight / 2.3))

    # renders everything on the game board
    def renderTetris(self):
        if self.gameboard.isGameOver():
            self.drawGameOver()
        elif self.gameboard.paused:
            self.drawGameboard()
            pauseText = UIVariables.fontBold.render("PAUSED", 1, (0, 0, 0))
            pauseScreen = pygame.Surface((Configs.gameboardWidth + 4, self.displayHeight)).convert_alpha()
            pauseScreen.fill((255, 255, 255, 230))
            self.gameDisplay.blit(pauseScreen, (0, 0))
            self.gameDisplay.blit(pauseText, (Configs.blockSize * 2.2, self.displayHeight / 2.3))
        else:
            self.clear()
            self.drawGameboard()
            self.drawCurrentPiece()
        pygame.display.update()


