import pygame
from view.gameView import GameView
from model.gameboard import Gameboard
from controller.gameController import GameController
from utils.variables import Configs


def main():
    columns = Configs.columns
    rows = Configs.rows
    name = Configs.name
    gameboard = Gameboard(columns, rows)
    gameView = GameView(name, gameboard, columns, rows)
    gameController = GameController(gameboard, gameView, columns, rows)
    # play the game
    gameController.playGame()


def start_game():
    pygame.mixer.pre_init(channels=32)
    pygame.init()
    main()
    pygame.quit()
    pygame.mixer.quit()


if __name__ == "__main__":
    start_game()
